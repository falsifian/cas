module Main where

import qualified CAS
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import Hash
import qualified MTree.BalancedBinaryTree as BBT
import MTree.CAS
import MTree.MTree
import qualified MTree.Set as Set
import Monad

casPath :: FilePath
casPath = "test_store"

elems :: [BS.ByteString]
elems = map (BSC.pack . show) ([0,2..98] ++ [1,3..99])

order :: Set.MOrder T CAS.Op
order a b = do ad <- getLabel a
               bd <- getLabel b
               return (compare ad bd)

addBS :: Set.MOrder T CAS.Op -> BS.ByteString -> T -> IO T
addBS order x s = do xn <- r $ construct x []
                     r $ Set.insert order xn s

memberBS :: Set.MOrder T CAS.Op -> BS.ByteString -> T -> IO Bool
memberBS order s x = do sn <- r $ construct s []
                        r $ Set.member order sn x

addList :: Set.MOrder T CAS.Op -> [BS.ByteString] -> T -> IO T
addList _ [] s = return s
addList order (h : t) s = addBS order h s >>= addList order t

deleteBS :: Set.MOrder T CAS.Op -> BS.ByteString -> T -> IO T
deleteBS order s x = do sn <- r $ construct s []
                        r $ Set.delete order sn x

deleteList :: Set.MOrder T CAS.Op -> [BS.ByteString] -> T -> IO T
deleteList _ [] s = return s
deleteList order (h : t) s = deleteBS order h s >>= deleteList order t

showsNode :: T -> CAS.Op (String -> String)
showsNode n = liftM shows (getLabel n)

r :: CAS.Op a -> IO a
r = flip CAS.run casPath

test_0 :: IO String
test_0 = do s <- r Set.empty
            False <- memberBS order (BSC.pack "50") s
            r $ BBT.verify order s
            s' <- addList order elems s
            r $ BBT.verify order s'
            True <- memberBS order (BSC.pack "51") s'
            True <- liftM (length elems ==) $ r $ Set.size s'
            s'' <- deleteBS order (BSC.pack "52") s'
            r $ BBT.verify order s''
            False <- memberBS order (BSC.pack "52") s''
            s''' <- deleteList order elems s''
            r $ BBT.verify order s'''
            0 <- r $ Set.size s'''
            False <- memberBS order (BSC.pack "53") s'''
            return "success"

tests :: [IO String]
tests = [test_0]

main :: IO ()
main = sequence_ [putStrLn ("test " ++ show i ++ "\n") >> test >>= putStrLn >> putStrLn "" | (i, test) <- zip [0 ..] tests]
