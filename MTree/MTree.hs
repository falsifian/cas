{-# LANGUAGE MultiParamTypeClasses #-}

module MTree.MTree
( MTree(construct, destruct, getLabel, getChildren)
, dump
, verify
) where

import Control.Monad
import qualified Data.ByteString as BS
import qualified Data.Set as Set

class Monad m => MTree node m where
    {-
        Minimum complete definition:
        construct and ((get_label and get_children) or destruct)
    -}
    construct :: BS.ByteString -> [node] -> m node
    destruct :: node -> m (BS.ByteString, [node])
    getLabel :: node -> m BS.ByteString
    getChildren :: node -> m [node]
    destruct t = 
        do l <- getLabel t
           c <- getChildren t
           return (l, c)
    getLabel = liftM fst . destruct
    getChildren = liftM snd . destruct

dump :: MTree node m => node -> m ShowS
dump =
    let d pre t =
            do (l, c) <- destruct t
               cs <- sequence (map (d (pre ++ " ")) c)
               return (((pre ++ "+") ++) . shows (BS.unpack l) . ("\n" ++) . foldr (.) id cs)
    in d ""

verify :: (MTree node m, Ord node) => [node] -> m ()
verify nodes =
    let f [] _visited = return ()
        f (node : nodes) visited
          | Set.member node visited = f nodes visited
          | True =
              do (_label, children) <- destruct node
                 f (children ++ nodes) (Set.insert node visited)
    in f nodes Set.empty
