{-# LANGUAGE MultiParamTypeClasses #-}

module Main where

import qualified CAS
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import Hash
import qualified MTree.BalancedBinaryTree as BBT
import MTree.CAS
import MTree.MTree
import qualified MTree.Map as Map
import Monad

casPath :: FilePath
casPath = "test_store"

pairs :: [(BS.ByteString, BS.ByteString)]
pairs = [(BSC.pack (show x), BSC.pack (show (x ^ 2))) | x <- [0,2..98] ++ [1,3..99]]

order :: Map.MOrder T CAS.Op
order a b = do ad <- getLabel a
               bd <- getLabel b
               return (compare ad bd)

addBSPair :: Map.MOrder T CAS.Op -> BS.ByteString -> BS.ByteString -> T -> IO T
addBSPair order key value m = r $
    do keyNode <- construct key []
       valueNode <- construct value []
       Map.insert order keyNode valueNode m

lookupBS :: Map.MOrder T CAS.Op -> BS.ByteString -> T -> IO (Maybe BS.ByteString)
lookupBS order key m = r $
    do keyNode <- construct key []
       mValueNode <- Map.lookup order keyNode m
       case mValueNode of
           Nothing -> return Nothing
           Just valueNode ->
               do (label, []) <- destruct valueNode
                  return (Just label)

addList :: Map.MOrder T CAS.Op -> [(BS.ByteString, BS.ByteString)] -> T -> IO T
addList _ [] s = return s
addList order ((hk, hv) : t) s = addBSPair order hk hv s >>= addList order t

deleteBS :: Map.MOrder T CAS.Op -> BS.ByteString -> T -> IO T
deleteBS order s x = do sn <- r $ construct s []
                        r $ Map.delete order sn x

deleteList :: Map.MOrder T CAS.Op -> [BS.ByteString] -> T -> IO T
deleteList _ [] s = return s
deleteList order (h : t) s = deleteBS order h s >>= deleteList order t

showsNode :: T -> CAS.Op (String -> String)
showsNode n =
    do (_label, [keyNode, valueNode]) <- destruct n
       key <- getLabel keyNode
       value <- getLabel valueNode
       return (\s -> "(" ++ BSC.unpack key ++ ", " ++ BSC.unpack value ++ ")" ++ s)

r :: CAS.Op a -> IO a
r = flip CAS.run casPath

test_0 :: IO String
test_0 = do s <- r Map.empty
            Nothing <- lookupBS order (BSC.pack "50") s
            r $ BBT.verify order s
            s' <- addList order pairs s
            r $ BBT.verify order s'
            putStrLn "OOM"
            do {str <- r $ BBT.dump showsNode s'; putStrLn (str "")}
            Just "2601" <- liftM (liftM BSC.unpack) $ lookupBS order (BSC.pack "51") s'
            putStrLn "MOO"
            True <- liftM (length pairs ==) $ r $ Map.size s'
            s'' <- deleteBS order (BSC.pack "52") s'
            r $ BBT.verify order s''
            Nothing <- lookupBS order (BSC.pack "52") s''
            s''' <- addBSPair order (BSC.pack "51") (BSC.pack "puppies") s''
            Just "puppies" <- liftM (liftM BSC.unpack) $ lookupBS order (BSC.pack "51") s'''
            s'''' <- deleteList order (map fst pairs) s'''
            r $ BBT.verify order s''''
            0 <- r $ Map.size s''''
            Nothing <- lookupBS order (BSC.pack "53") s''''
            return "success"

tests :: [IO String]
tests = [test_0]

main :: IO ()
main = sequence_ [putStrLn ("test " ++ show i ++ "\n") >> test >>= putStrLn >> putStrLn "" | (i, test) <- zip [0 ..] tests]
