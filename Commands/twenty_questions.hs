module Main where

import CASDAG
import qualified Data.ByteString as BS
import Hash
import IO
import System
import Util

main :: IO ()
main =
	do	hSetBuffering stdout NoBuffering
		[cas] <- getArgs
		putStr "root hash? "
		getLine >>= play cas [] . stringToHash0

play :: CASSpec -> [(BS.ByteString, Bool, Hash0)] -> Hash0 -> IO ()
play cas history node_hash = run (read_node node_hash >>= map_fst_M read_data) cas >>= \ (content, child_hashes) -> case child_hashes of
	[] -> 
		do	putStr "Is it a "
			BS.putStr content
			putStr "? "
			answer <- getBool
			if answer then (return ()) else
				do	putStrLn "What is it?"
					new_object <- BS.getLine
					putStr "Enter a question to distinguish "
					BS.putStr content
					putStr " from "
					BS.putStr new_object
					putStrLn "."
					new_question <- BS.getLine
					putStr "Is the answer yes for "
					BS.putStr new_object
					putStr "? "
					new_answer <- getBool
					new_root <-
							run (add_data new_object >>= flip make [] >>= update ((new_question, new_answer, node_hash) : history)) cas
					putStrLn ("New root: " ++ hash0ToString new_root ++ ".")
	[yes_child, no_child] ->
		do	BS.putStrLn content
			answer <- getBool
			(follow_child, other_child) <- return (if answer then (yes_child, no_child) else (no_child, yes_child))
			play cas ((content, answer, other_child) : history) follow_child

update :: [(BS.ByteString, Bool, Hash0)] -> Hash0 -> Op Hash0
update [] root = return root
update ((question, change_answer, other_child) : history) new_child =
	let	(yes_child, no_child) = if change_answer then (new_child, other_child) else (other_child, new_child)
	in
	do	node_hash <- add_data question >>= flip make [yes_child, no_child]
		update history node_hash

getBool :: IO Bool
getBool = getLine >>= \ answer -> case answer of
	"yes" -> return True
	"no" -> return False
	_ -> putStrLn "Please enter \"yes\" or \"no\"." >> getBool
