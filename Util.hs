module Util (P0(P0, p0_destruct), Compose(Compose, compose_destruct), Compose_ATT(Compose_ATT, compose_att_destruct), apply, escape_for_stdout, cmds_main, map_fst_M) where

import Data.Char
import Data.Maybe
import System.Environment

newtype Compose a b c = Compose { compose_destruct :: a (b c) }

newtype Compose_ATT a b c d = Compose_ATT { compose_att_destruct :: a (b c) d }

newtype P0 a b = P0 { p0_destruct :: a }

apply :: (a -> b) -> a -> b
apply f x = f x

escape_for_stdout :: String -> String
escape_for_stdout = map (\ c -> if isAscii c && isAlphaNum c then c else '_')

cmds_main :: (String -> Maybe ([String] -> IO ())) -> IO ()
cmds_main cmds = getArgs >>= procArgs
	where
		procArgs [] = fail "first arg must be a command"
		procArgs (cmd : args) = fromMaybe
			(const (fail ("unknown command: " ++ escape_for_stdout cmd)))
			(cmds cmd)
			args

map_fst_M :: (Monad m) => (a -> m b) -> (a, c) -> m (b, c)
map_fst_M f (x, y) = f x >>= \ x' -> return (x', y)
